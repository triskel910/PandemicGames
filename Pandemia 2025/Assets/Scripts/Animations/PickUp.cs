﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Player.cantmove)
        {
            GetComponent<Animation>().Play("Pickup");
        }
        if (!Player.cantmove)
        {
            //GetComponent<Animation>().Stop("Pickup");
            GetComponent<Animation>().Rewind("Pickup");
        }
    }
}
