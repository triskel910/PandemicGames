﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieStates : MonoBehaviour
{
    private NavMeshAgent nav;
    private Vector3 target;
    public Transform idle;

    public Animator anims;

    public static bool Insight;

    public static bool ZDetect;
    public static bool playerpos;

    private bool haveSeen = false;

    private bool savePlayerPos = false;
    private Vector3 lastSpot;
    private Transform lastSeen;

    private float timer;

    private GameObject player;
    public GameObject lastPlayerpos;

    private Vector3 currentPos;

    private float x, y, z;

   /* public GameObject walkAnim;
    public GameObject idleAnim;
    public GameObject punchAnim;*/
    private float timeAttack = 0.0f;

    private bool isAttacking = false;

    private float stopdoing = 0.0f;
    public void Start()
    {
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        // lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");
        nav = GetComponent<NavMeshAgent>();

       /* idleAnim.SetActive(true);
        walkAnim.SetActive(false);
        punchAnim.SetActive(false);*/

        Insight = false;
        ZDetect = false;
        isAttacking = false;
        playerpos = false;

        //target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);

        //GetComponent<NavMeshAgent>().enabled = true;
    }
    private void Update()
    {
        target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        /*if(haveSeen && !Insight && playerpos)
            lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");*/

        //when it stops
        if (nav.remainingDistance <= nav.stoppingDistance)
        {

            if (!nav.hasPath || Mathf.Abs(nav.velocity.sqrMagnitude) < float.Epsilon)
            {

                /* walkAnim.SetActive(false);
                 idleAnim.SetActive(true);*/
                anims.GetComponent<Animator>().SetBool("Zwalk", false);
            }

        }
        else
        {
            anims.GetComponent<Animator>().SetBool("Zwalk", true);
            /*   walkAnim.SetActive(true);
               idleAnim.SetActive(false);*/
        }


        /*if (GameManager.Instance.restartZombiesLevel2)
        {
            Start();
            nav.SetDestination(idle.position);
        }*/
        attackPlayer();



        if (ZDetect)
        {
            haveSeen = true;
            //savePlayerPos = true;
        }

        /*if(savePlayerPos)
            lastSeen = target;*/

        if (savePlayerPos)
        {
            lastSpot = target;
            savePlayerPos = false;
        }


        if (Insight && ZDetect)
        {
            /*if (!isAttacking)
            {
                idleAnim.SetActive(false);
                walkAnim.SetActive(true);
            }*/
            nav.SetDestination(target);
        }
        if (!Insight && haveSeen)
        {
            //savePlayerPos = true;
            //lastSeen = lastSpot;



            lastSpot = lastPlayerpos.transform.position;

            nav.SetDestination(lastSpot);

            timer += Time.fixedDeltaTime;


            if (timer >= 30.0f)
            {

                nav.SetDestination(idle.position);
                ZDetect = false;
                haveSeen = false;
                timer = 0.0f;
            }
        }
        if (GameManager.Instance.playerplaydead)
        {

            nav.SetDestination(idle.position);
            ZDetect = false;
            haveSeen = false;
            savePlayerPos = false;
            //punchAnim.SetActive(false);
            isAttacking = false;
            //stopdoing += Time.deltaTime;

            /*if (stopdoing > 2.0f)
            {
                idleAnim.SetActive(true);
                punchAnim.SetActive(false);
            }*/

        }



    }

    private void attackPlayer()
    {
        if (ZombieAttacklevel.canattack)
        {
            isAttacking = true;
        /*    idleAnim.SetActive(false);
            walkAnim.SetActive(false);
            punchAnim.SetActive(true);*/

            GameManager.Instance.playerplaydead = true;

            /* timeAttack += Time.deltaTime;
             if (timeAttack == 2.10f && ZombieAttacklevel3.canattack)
             {
                 GameManager.Instance.playerplaydead = true;
             }*/
        }
        if (!ZombieAttacklevel.canattack)
        {
            //timeAttack += Time.deltaTime * 0;
            isAttacking = false;

        }


    }
    /* public void savePos(float x,float y, float z)
        {
            x = player.transform.position.x;
            y = player.transform.position.y;
            z = player.transform.position.z;

        }*/
}