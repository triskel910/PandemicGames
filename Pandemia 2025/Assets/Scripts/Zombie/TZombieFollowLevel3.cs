﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TZombieFollowLevel3 : MonoBehaviour {

    public GameObject lastPlayerpos;
    private GameObject player;

    private GameObject Dog;
   // private GameObject lastDogpos;

    // Use this for initialization
    void Start () {
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");

        Dog = (GameObject)GameObject.FindGameObjectWithTag("Dog");
        //lastDogpos = (GameObject)GameObject.FindGameObjectWithTag("LastDogpos");
        //lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ZombieStatesLevel3.Insight = true;
            ZombieStatesLevel3.playerpos = false;
           /* if (lastPlayerpos != null)
                Destroy(lastPlayerpos);*/
        }

        if (other.gameObject.tag == "Dog")
        {
            ZombieStatesLevel3.canfollowBark = true;
           // lastDogpos.transform.position = new Vector3(Dog.transform.position.x, Dog.transform.position.y, Dog.transform.position.z);
        }
    }

    private void OnTriggerExit(Collider exit)
    {
        if (exit.gameObject.tag == "Player")
        {
            ZombieStatesLevel3.Insight = false;
            ZombieStatesLevel3.playerpos = true;
           /* if(lastPlayerpos == null)
            {
                lastPlayerpos  = Instantiate(Resources.Load("lastPlayerpos")) as GameObject;
                
            }*/
            lastPlayerpos.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        }
        if (exit.gameObject.tag == "Dog")
        {
            ZombieStatesLevel3.canfollowBark = false;
        }
    }
}
