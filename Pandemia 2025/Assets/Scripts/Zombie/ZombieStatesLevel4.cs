﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieStatesLevel4 : MonoBehaviour
{
    private NavMeshAgent nav;
    private Vector3 target;
    public Transform idle;

    public static bool Insight;
    
    public static bool ZDetect;
    public static bool playerpos;

    private bool haveSeen = false;

    private bool savePlayerPos = false;
    private Vector3 lastSpot;
    private Transform lastSeen;

    private float timer;

    private GameObject player;
    public GameObject lastPlayerpos;

    private Vector3 currentPos;

    private float x, y, z;

    public GameObject walkAnim;
    public GameObject idleAnim;
    public GameObject punchAnim;
    private float timeAttack = 0.0f;

    private bool isAttacking = false;

    private float stopdoing = 0.0f;
    private void Start()
    {
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<NavMeshAgent>();

        idleAnim.SetActive(true);
        walkAnim.SetActive(false);
        punchAnim.SetActive(false);

        Insight = false;
        ZDetect = false;
        isAttacking = false;
        playerpos = false;


        GetComponent<NavMeshAgent>().enabled = true;

    }
    private void Update()
    {

        target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        //when it stops
        if (nav.remainingDistance <= nav.stoppingDistance)
        {

            if (!nav.hasPath || Mathf.Abs(nav.velocity.sqrMagnitude) < float.Epsilon)
            {

                walkAnim.SetActive(false);
                idleAnim.SetActive(true);
            }

        }
        else
        {

            walkAnim.SetActive(true);
            idleAnim.SetActive(false);
        }
        /*if(haveSeen && !Insight && playerpos)
            lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");*/
            
        attackPlayer();



        if (ZDetect)
        {
            haveSeen = true;
            //savePlayerPos = true;
        }

        /*if(savePlayerPos)
            lastSeen = target;*/

        if (savePlayerPos)
        {
            lastSpot = target;
            savePlayerPos = false;
        }

        
        if (Insight && ZDetect)
        {
            if (!isAttacking)
            {
                idleAnim.SetActive(false);
                walkAnim.SetActive(true);
            }
            nav.SetDestination(target);
        }
        if (!Insight && haveSeen)
        {
            //savePlayerPos = true;
            //lastSeen = lastSpot;



            lastSpot = lastPlayerpos.transform.position;

            nav.SetDestination(lastSpot);

            timer += Time.fixedDeltaTime;
            

            if (timer >= 30.0f)
            {
               
                nav.SetDestination(idle.position);
                ZDetect = false;
                haveSeen = false;
                timer = 0.0f;
            }
        }
        if(GameManager.Instance.playerplaydead)
        {

            GetComponent<NavMeshAgent>().enabled = false;
            isAttacking = false;
            stopdoing += Time.deltaTime;

            if (stopdoing > 2.0f)
            {
                idleAnim.SetActive(true);
                punchAnim.SetActive(false);
            }

        }


        
    }

    private void attackPlayer()
    {
        if (ZombieAttacklevel4.canattack)
        {
            isAttacking = true;
            idleAnim.SetActive(false);
            walkAnim.SetActive(false);
            punchAnim.SetActive(true);

            GameManager.Instance.playerplaydead = true;

           /* timeAttack += Time.deltaTime;
            if (timeAttack == 2.10f && ZombieAttacklevel3.canattack)
            {
                GameManager.Instance.playerplaydead = true;
            }*/
        }
        if (!ZombieAttacklevel4.canattack)
        {
            //timeAttack += Time.deltaTime * 0;
            isAttacking = false;

        }      


    }
    /* public void savePos(float x,float y, float z)
        {
            x = player.transform.position.x;
            y = player.transform.position.y;
            z = player.transform.position.z;

        }*/
}
