﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrawlerAttack : MonoBehaviour {

    public static bool canattack;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!GameManager.Instance.playerplaydead)
                canattack = true;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            canattack = false;
    }
}
