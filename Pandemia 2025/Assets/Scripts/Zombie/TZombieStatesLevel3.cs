﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TZombieStatesLevel3 : MonoBehaviour
{
    private NavMeshAgent nav;
    private Vector3 target;
    public Transform idle;

    public static bool Insight;
    
    public static bool ZDetect;
    public static bool playerpos;

    private bool haveSeen = false;

    private bool savePlayerPos = false;
    private Vector3 lastSpot;
    private Transform lastSeen;

    private float timer;

    private GameObject player;
    public GameObject lastPlayerpos;

    private Vector3 currentPos;

    private float x, y, z;

    public GameObject walkAnim;
    public GameObject idleAnim;
    public GameObject punchAnim;
    private float timeAttack = 0.0f;

    private bool isAttacking = false;

    public static bool canfollowBark;
    public static bool followBark = false;

    private Vector3 lastDogspot;
    public GameObject lastDogpos;

    private float stopdoing = 0.0f;
    private void Start()
    {
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
       // lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");
        nav = GetComponent<NavMeshAgent>();

        idleAnim.SetActive(true);
        walkAnim.SetActive(false);
        punchAnim.SetActive(false);
       

        if(lastDogpos==null)
            lastDogpos = (GameObject)GameObject.FindGameObjectWithTag("LastDogpos");
    }
    private void Update()
    {
        //when it stops
        if (nav.remainingDistance <= nav.stoppingDistance)
        {

            if (!nav.hasPath || Mathf.Abs(nav.velocity.sqrMagnitude) < float.Epsilon)
            {

                walkAnim.SetActive(false);
                idleAnim.SetActive(true);
            }

        }
        else
        {

            walkAnim.SetActive(true);
            idleAnim.SetActive(false);
        }

        /*if(haveSeen && !Insight && playerpos)
            lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");*/

        target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);

        attackPlayer();



        if (ZDetect)
        {
            haveSeen = true;
            //savePlayerPos = true;
        }

        /*if(savePlayerPos)
            lastSeen = target;*/

        if (savePlayerPos)
        {
            lastSpot = target;
            savePlayerPos = false;
        }

        
        if (Insight && ZDetect)
        {
           /* if (!isAttacking)
            {
                idleAnim.SetActive(false);
                walkAnim.SetActive(true);
            }*/
            nav.SetDestination(target);
        }
        if (!Insight && haveSeen)
        {
            //savePlayerPos = true;
            //lastSeen = lastSpot;



            lastSpot = lastPlayerpos.transform.position;

            nav.SetDestination(lastSpot);

            timer += Time.fixedDeltaTime;
            

            if (timer >= 30.0f)
            {
               
                nav.SetDestination(idle.position);
                ZDetect = false;
                haveSeen = false;
                timer = 0.0f;
            }
        }

        if(!Insight && followBark)
        {

           // walkAnim.SetActive(true);

            lastDogspot = lastDogpos.transform.position;
            nav.SetDestination(lastDogspot);

            
           
        }
            

        if (GameManager.Instance.playerplaydead)
        {

            GetComponent<NavMeshAgent>().enabled = false;
            isAttacking = false;
            stopdoing += Time.deltaTime;

            if (stopdoing > 2.0f)
            {
                idleAnim.SetActive(true);
                punchAnim.SetActive(false);
            }

        }


        
    }

    private void attackPlayer()
    {
        if (ZombieAttacklevel3.canattack)
        {
            isAttacking = true;
            idleAnim.SetActive(false);
            walkAnim.SetActive(false);
            punchAnim.SetActive(true);

            GameManager.Instance.playerplaydead = true;

           /* timeAttack += Time.deltaTime;
            if (timeAttack == 2.10f && ZombieAttacklevel3.canattack)
            {
                GameManager.Instance.playerplaydead = true;
            }*/
        }
        if (!ZombieAttacklevel3.canattack)
        {
            //timeAttack += Time.deltaTime * 0;
            isAttacking = false;

        }      


    }
    
}
