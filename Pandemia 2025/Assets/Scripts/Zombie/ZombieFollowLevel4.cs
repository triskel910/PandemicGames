﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieFollowLevel4 : MonoBehaviour {

    public GameObject lastPlayerpos;
    private GameObject player;
    // Use this for initialization
    void Start () {
        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        //lastPlayerpos = (GameObject)GameObject.FindGameObjectWithTag("LastPlayerpos");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ZombieStatesLevel4.Insight = true;
            ZombieStatesLevel4.playerpos = false;
           /* if (lastPlayerpos != null)
                Destroy(lastPlayerpos);*/
        }
    }

    private void OnTriggerExit(Collider exit)
    {
        if (exit.gameObject.tag == "Player")
        {
            ZombieStatesLevel4.Insight = false;
            ZombieStatesLevel4.playerpos = true;
           /* if(lastPlayerpos == null)
            {
                lastPlayerpos  = Instantiate(Resources.Load("lastPlayerpos")) as GameObject;
                
            }*/
            lastPlayerpos.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        }
    }
}
