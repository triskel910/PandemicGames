﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurningScript : MonoBehaviour {
    // The target marker.
    public Transform targetA;
    public Transform targetD;
    public Transform targetW;
    public Transform targetS;
    public Transform targetSA;
    public Transform targetSD;
    public Transform targetWA;
    public Transform targetWD;
    float angle;
    // Angular speed in radians per sec.
    float speed = 3.0f;
    private Transform currentTarget;

    private void Start()
    {
        currentTarget = targetW.transform;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            currentTarget = targetA.transform;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            currentTarget = targetD.transform;
        }
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            currentTarget = targetW.transform;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            currentTarget = targetS.transform;
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W) ||  Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.UpArrow))
        {
            currentTarget = targetWA.transform;
        }
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.UpArrow))
        {
            currentTarget = targetWD.transform;
        }
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.DownArrow))
        {
            currentTarget = targetSD.transform;
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S)|| Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.DownArrow))
        {
            currentTarget = targetSA.transform;

        }



        Vector3 relative = transform.InverseTransformPoint(currentTarget.position);
        angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg * Time.deltaTime * speed;
        transform.Rotate(0, angle, 0);


    }

}