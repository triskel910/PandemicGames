﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Dog : MonoBehaviour
{

    private GameObject Player;
    private GameObject mueble1;
    private GameObject mueble2;
    private GameObject muebleLlave;

 

    public float movSpeed;
    public float rotSpeed;

    public float pSpeed;
    public float pRotation;

    private Transform dPos;
    private Transform target;

    public static bool stopFollow = false;
    public static bool follow;
    public static bool search;
    public static bool isControlled = false;

    public static bool isRoom5 = false;
    public static bool isRoom3 = false;
    private float searchTimer;

    public GameObject key;
    private GameObject cKey;
    private float force = 500.0f;
    private float forced = 400.0f;

    private NavMeshAgent theAgent;

    public static bool teleport = false;
    private float tteleport = 0.0f;

    private DogAnimations doAnims;
    public Animator Animations;

    public static bool barkE = false;
    public GameObject lastDogpos;


    private float nextTimetoBark = 0.0f;
    private float barkrate = 0.2f;

    CharacterController charControl;
    public float walkSpeed;

    public GameObject Excl;

    Vector3 lastPosition = Vector3.zero;
    private float speedCheck;

    private void Awake()
    {
        dPos = transform;
        charControl = GetComponent<CharacterController>();
    }
    // Use this for initialization
    void Start()
    {

        Excl.SetActive(false);

        theAgent = GetComponent<NavMeshAgent>();
        gameObject.GetComponent<NavMeshAgent>().enabled = true;

        Player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        mueble1 = (GameObject)GameObject.FindGameObjectWithTag("Mueble1");
        mueble2 = (GameObject)GameObject.FindGameObjectWithTag("Mueble2");
        muebleLlave = (GameObject)GameObject.FindGameObjectWithTag("MuebleConLlave");

       
        search = false;
        follow = true;
        searchTimer = 0.0f;
        stopFollow = false;

        target = Player.transform;
        movSpeed = 3.0f;


        doAnims = gameObject.GetComponent<DogAnimations>();

        //lastDogpos = (GameObject)GameObject.FindGameObjectWithTag("LastDogpos");

        GetComponent<NavMeshAgent>().stoppingDistance = 1.8f;

    }
    private void FixedUpdate()
    {

        speedCheck = (transform.position - lastPosition).magnitude;
        lastPosition = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        Physics.IgnoreCollision(Player.GetComponent<Collider>(), GetComponent<Collider>());

        if (theAgent.remainingDistance <= theAgent.stoppingDistance)
        {

            if (!theAgent.hasPath || Mathf.Abs(theAgent.velocity.sqrMagnitude) < float.Epsilon)
            {
                
                    Animations.SetBool("DogWalk",false);
                              
            }

        }
        else
        {
            Animations.SetBool("DogWalk", true);

        }


        if (teleport)
        {
            gameObject.GetComponent<NavMeshAgent>().enabled = false;
            if (follow)
                teleport = false;
        
        }



        if (!stopFollow)
        {
            target = Player.transform;
            this.GetComponent<NavMeshAgent>().speed = 2.0f;
            if (!teleport)
                gameObject.GetComponent<NavMeshAgent>().enabled = true;
        }

        //Debug.Log(searchTimer + " Time");

        if (follow)
        {
            if (!isControlled && !search)
            {
                
                theAgent.SetDestination(target.position);
              
            }
        }
    
        if (stopFollow )
        {
            if(!search)
                GetComponent<NavMeshAgent>().speed = 0.0f;
           // doAnims.PlayAnimations(1);
            
            /*   dPos.rotation = Quaternion.Slerp(dPos.rotation, Quaternion.LookRotation(target.position - dPos.position), rotSpeed * Time.deltaTime);
               dPos.position += dPos.forward * movSpeed * Time.deltaTime;*/
        }

        SearchForItems();

        MoveDog();

        Bark();

       
    }

    private void MoveDog()
    {
        if (GameManager.Instance.switchCharacter)
        {
            gameObject.GetComponent<NavMeshAgent>().enabled = false;
            

             if (Input.GetKey(KeyCode.D))
             {
                 transform.Rotate(new Vector3(0, pRotation * Time.deltaTime, 0));

             }
             if (Input.GetKey(KeyCode.A))
             {
                 transform.Rotate(new Vector3(0, -pRotation * Time.deltaTime, 0));

             }
           
            float vert = Input.GetAxis("Vertical");

           
            Vector3 moveDirForward = transform.forward * vert * walkSpeed;

           
            charControl.SimpleMove(moveDirForward);

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(!search)
                follow = false;
            //Debug.Log("isOut");
        }

        if (other.gameObject.tag == "TriggerH5")
        {
            isRoom5 = true;
            Excl.SetActive(true);
        }

        if (other.gameObject.tag == "TriggerH3")
            isRoom3 = true;

       

        if (other.gameObject.tag == "Mueble1")
        {

          

        }
        if (other.gameObject.tag == "Mueble2")
        {

           
        }
        

        if (other.gameObject.tag == "MuebleConLlave")
        {
          
        }


    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
           // Debug.Log("is out");
            follow = true;
        }

        if (other.gameObject.tag == "TriggerH5")
        {
            isRoom5 = false;
            Excl.SetActive(false);
            // GetComponent<Rigidbody>().freezeRotation = false;
        }

        if (other.gameObject.tag == "TriggerH3")
            isRoom3 = false;

        if (other.gameObject.tag == "Mueble1")
        {

         

        }
        if (other.gameObject.tag == "Mueble2")
        {

          
        }


        if (other.gameObject.tag == "MuebleConLlave")
        {
           
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
           // follow = false;
          //  gameObject.GetComponent<Rigidbody>().AddRelativeForce(transform.forward * -forced);
        }
    }

    public void SearchForItems()
    {
        if (search)
        {
            Excl.SetActive(false);
            stopFollow = true;           
            searchTimer += Time.fixedDeltaTime;
            theAgent.SetDestination(target.position);
            GetComponent<NavMeshAgent>().stoppingDistance = 0;
            //gameObject.GetComponent<NavMeshAgent>().enabled = false;

            /*  dPos.rotation = Quaternion.Slerp(dPos.rotation, Quaternion.LookRotation(target.position - dPos.position), rotSpeed * Time.deltaTime);
              dPos.position += dPos.forward * movSpeed * Time.deltaTime;*/


            if (isRoom5)
            {

                if (searchTimer >= 0.0f)
                {
                    target = mueble1.transform;
                   
                }
                if (searchTimer >= 3.0f)
                {
                   
                    target = mueble2.transform;
                    
                }

                if (searchTimer >= 7.0f)
                {
                    target = muebleLlave.transform;
                   
                    
                }
                if (searchTimer >= 14.0f)
                {
                    

                    if (GameManager.Instance.respawnKey)
                    {
                        cKey = Instantiate(key);
                        cKey.gameObject.GetComponent<Rigidbody>().AddForce(transform.up * force);
                        cKey.gameObject.transform.rotation = Quaternion.Euler(new Vector3(2 * Time.deltaTime, 0, 0));
                        //cKey.gameObject.transform.position = (new Vector3(0.0f, 1.0f * Time.deltaTime, 0.0f));
                        Physics.IgnoreCollision(cKey.GetComponent<Collider>(), this.GetComponent<Collider>());
                        Destroy(cKey, 1.2f);
                    }
                    GameManager.Instance.respawnKey = false;
                    GameManager.Instance.haveKeyFromRoom5 = true;
                    Start();
                }
            }
           


        }
    }

    public void Bark()
    {
        if (GameManager.Instance.switchCharacter)
        {
            if (Input.GetKeyDown(KeyCode.E) && Time.time >= nextTimetoBark)
            {
                nextTimetoBark = Time.time + 1f / barkrate;
                lastDogpos.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

                if (ZombieStatesLevel3.canfollowBark)
                    ZombieStatesLevel3.followBark = true;
            }

        }
    }

    void pushBack()
    {

    }
}