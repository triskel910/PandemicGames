﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogAnimations : MonoBehaviour {


    

  

    void Start () {
		this.gameObject.GetComponent<Animation>().Play("idle");

        
	}

    // Update is called once per frame
    void Update()
    {

        if (!GameManager.Instance.switchCharacter)
        {
            if (Dog.stopFollow)
            {
                this.gameObject.GetComponent<Animation>().Play("idle");
                this.gameObject.GetComponent<Animation>().Stop("walk");
            }

            if (Dog.follow)
            {
                this.gameObject.GetComponent<Animation>().Play("walk");
                this.gameObject.GetComponent<Animation>().Stop("idle");
            }


        }else if(GameManager.Instance.switchCharacter)
        {
            if (Input.GetKey(KeyCode.W))
            {
                this.gameObject.GetComponent<Animation>().Play("walk");
                this.gameObject.GetComponent<Animation>().Stop("idle");
            }
        }
            else if(Input.GetKeyUp(KeyCode.W))
            {
                this.gameObject.GetComponent<Animation>().Play("idle");
                this.gameObject.GetComponent<Animation>().Stop("walk");
            }
        }
    }

   

