﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayDeathsound : MonoBehaviour {

    public AudioClip death;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(GameManager.Instance.playerplaydead)
            GetComponent<AudioSource>().PlayOneShot(death, 1.0f);

    }
}
