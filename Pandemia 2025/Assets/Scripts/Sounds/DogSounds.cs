﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogSounds : MonoBehaviour {

    public AudioClip bark;
    public AudioClip barkAttrack;
    private  bool barkonce = false;
    private bool barkonceT = false;
    private float nextTimetoBark = 0.0f;
    private float barkrate = 0.2f;
    

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {


        if (barkonce)
        {
            GetComponent<AudioSource>().PlayOneShot(bark, 1.0f);
            barkonce = false;
        }

        if (barkonceT)
        {
            GetComponent<AudioSource>().PlayOneShot(barkAttrack, 1.0f);
            barkonceT = false;
        }

        if (!GameManager.Instance.switchCharacter)
        {
            if (Input.GetKeyDown(KeyCode.T))
                barkonce = true;
        }

        if (GameManager.Instance.switchCharacter)
        {
            if (Input.GetKeyDown(KeyCode.E) && Time.time >= nextTimetoBark)
            {
                nextTimetoBark = Time.time + 1f / barkrate;
                barkonceT = true;
            }
        }
    }

    
       
}
