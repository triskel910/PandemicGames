﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohnSteps : MonoBehaviour {

    
    private bool once = false;

    private float currentSpeed;

    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<AudioSource>().loop = true;
        currentSpeed = GetComponent<Rigidbody>().velocity.magnitude;

          if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
          {
              if (!GameManager.Instance.freezeTime)
              {
                  if (!Dog.isControlled)
                      GetComponent<AudioSource>().Play();
              }
          }

       

        if (currentSpeed < 0.1)
        {
            GetComponent<AudioSource>().Stop();
           
        }
        
    }
       
}
