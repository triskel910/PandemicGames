﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSound : MonoBehaviour {

    public AudioClip knock;
    public static bool onceKnock = false;

    public AudioClip locked;
    public static bool onceLocked = false;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (onceKnock)
        {
            GetComponent<AudioSource>().PlayOneShot(knock, 1.0f);
            onceKnock = false;
        }

        if (onceLocked)
        {
            GetComponent<AudioSource>().PlayOneShot(locked, 1.0f);
            onceLocked = false;
        }
    }

   
}