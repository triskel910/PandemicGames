﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralSounds : MonoBehaviour {


    private AudioSource audioS;
    public GameObject level1;
    public GameObject level2;
    public GameObject level3;
    public GameObject level4;

    private bool playMusic2 = true;

    // Use this for initialization
    void Start()
    {
        level2.SetActive(false);
        level3.SetActive(false);
        level4.SetActive(false);
    }
	// Update is called once per frame
	void Update ()
    {
        if(GameManager.Instance.isLevel2)
        {
            level1.SetActive(false);
            level2.SetActive(true);
        }
        if (GameManager.Instance.isLevel3)
        {
            level2.SetActive(false);
            level3.SetActive(true);
        }
        if (GameManager.Instance.isLevel4)
        {
            level3.SetActive(false);
            level4.SetActive(true);
        }





    }
   
}
