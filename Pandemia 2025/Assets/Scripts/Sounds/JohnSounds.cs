﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohnSounds : MonoBehaviour {

    public AudioClip coff;
    private bool once = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (once)
        {
            GetComponent<AudioSource>().PlayOneShot(coff, 1.0f);
            once = false;
        }


        if (Input.GetKeyDown(KeyCode.R))
            once = true;

    }
}
