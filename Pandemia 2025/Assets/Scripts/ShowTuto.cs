﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTuto : MonoBehaviour {

    private float timer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timer += Time.deltaTime;

        if (timer >= 10.0f)
        {
            gameObject.SetActive(false);
            Destroy(this.gameObject);
        }
	}
}
