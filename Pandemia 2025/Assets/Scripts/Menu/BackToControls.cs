﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToControls : MonoBehaviour {

    public GameObject showSettings;
    public GameObject showControls;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        showControls.SetActive(false);
        showSettings.SetActive(true);
    }
}
