﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayLoading : MonoBehaviour {


    public static bool playVid = false;
    public VideoPlayer video;
   
    // Use this for initialization
    void Start () { 
        video = GetComponent<VideoPlayer>();
        video.frame = 0;
    }
	
	// Update is called once per frame
	void Update () {
        video.loopPointReached += CheckOver;
        if (playVid)
        {
            
            GetComponent<VideoPlayer>().enabled = true;
            
            video.Play();
            
        }
        if (!playVid)
        {
            GetComponent<VideoPlayer>().enabled = false;
            video.Stop();
        }

    }
    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        playVid = false;
    }
}
