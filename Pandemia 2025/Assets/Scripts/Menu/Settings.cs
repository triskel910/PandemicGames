﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {

    public GameObject showSettings;
    public GameObject showMenu;
	// Use this for initialization
	void Start () {
        showSettings.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnMouseDown()
    {
        showMenu.SetActive(false);
        showSettings.SetActive(true);
    }
}
