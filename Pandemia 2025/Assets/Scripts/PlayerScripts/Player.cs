﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float playerSpeed;
    public float playerRotation;
    private Dog DogFunctions;

    //puertas
    private GameObject Ph7;
    private GameObject Ph6;
    private GameObject Ph5;
    private GameObject Ph4;
    private GameObject Ph3;
    private GameObject Ph2;
    private GameObject Ph1;

    private GameObject th7;
    private GameObject th6;
    private GameObject th5;
    private GameObject th4;
    private GameObject th3;
    private GameObject th1;

    public Animator playerAnims;
   
    
    
    public static bool cantmove = false;

    private float timerPick;
    private float timerPick2;
    private float timerPick3;
    private float timerPick4;
    private float timerPick5;
    private float timerPickkey42;

    private bool isPickingN1 = false;
    private bool isPickingN2 = false;
    private bool isPickingN3 = false;
    private bool isPickingN4 = false;
    private bool isPickingN5 = false;

    private bool canPickupN1;
    private bool canPickupN2;
    private bool canPickupN3;
    private bool canPickupN4;
    private bool canPickupN5;

    private bool canOpen1;
    private bool canOpen2;
    private bool canOpen3;
    private bool canOpen4;
    private bool canOpen5;
    private bool canOpen6;
    private bool canOpen7;

    private bool canClose1;
    private bool canClose3;
    private bool canClose4;
    private bool canClose5;
    private bool canClose6;
    private bool canClose7;

    private bool door1IsOpen;
    private bool door2IsOpen;
    private bool door3IsOpen;
    private bool door4IsOpen;
    private bool door5IsOpen;
    private bool door6IsOpen;
    private bool door7IsOpen;

    //level2
    private bool canOpen2dr1;
    private bool canOpen2dr2;
    private bool canOpen2dr3;
    private bool canOpen2dr4;
    private GameObject l2dr1;
    private GameObject l2dr2;
    private GameObject l2dr3;
    private GameObject l2dr4;

    private GameObject puerta42;
    private GameObject puertaCura;
    private bool canopen42 = false;
    private bool canopenCura = false;


    private GameObject nota1;
    private GameObject nota2;
    private GameObject nota3;
    private GameObject nota4;
    private GameObject nota5;

    // public GameObject textCode;
    //Bolean Puertas
    private bool OpenPh2 = false;


    //gui Notes
    public GameObject noteGui1;
    public GameObject noteGui2;
    public GameObject noteGui3;
    public GameObject noteGui4;
    public GameObject noteGui5;

    public GameObject noteGui1B;
    public GameObject noteGui2B;
    public GameObject noteGui3B;
    public GameObject noteGui4B;
    public GameObject noteGui5B;


    public GameObject LabKey;
    private bool cangrabLabkey = false;

    public GameObject key41;
    public GameObject key42;
    private bool cangrabkey41 = false;
    private bool cangrabkey42 = false;


    private bool cangrabCura = false;
    private GameObject cura;

    private GameObject PickUpAnim;
    private GameObject PickAnim;

    private bool canplaydoor = false;

    private float speed = 2.0F;
    private float currentSpeed;
    

    private Vector3 moveDirection = Vector3.zero;

    public GameObject JohnModel;
    private float minDistance = 1.5f;

    private float timeD = 0.0f;

    Vector3 lastPosition = Vector3.zero;
    float speedCheck;

    public GameObject TriggerBoard;
    public GameObject rex;

    // Use this for initialization
    void Start()
    {
        Ph7 = (GameObject)GameObject.FindGameObjectWithTag("Ph7");
        Ph6 = (GameObject)GameObject.FindGameObjectWithTag("Ph6");
        Ph5 = (GameObject)GameObject.FindGameObjectWithTag("Ph5");
        Ph4 = (GameObject)GameObject.FindGameObjectWithTag("Ph4");
        Ph3 = (GameObject)GameObject.FindGameObjectWithTag("Ph3");
        Ph2 = (GameObject)GameObject.FindGameObjectWithTag("Ph2");
        Ph1 = (GameObject)GameObject.FindGameObjectWithTag("Ph1");

        th7 = (GameObject)GameObject.FindGameObjectWithTag("Th7");
        th6 = (GameObject)GameObject.FindGameObjectWithTag("Th6");
        th5 = (GameObject)GameObject.FindGameObjectWithTag("Th5");
        th4 = (GameObject)GameObject.FindGameObjectWithTag("Th4");
        th3 = (GameObject)GameObject.FindGameObjectWithTag("Th3");
        th1 = (GameObject)GameObject.FindGameObjectWithTag("Th1");



      


        DogFunctions = FindObjectOfType<Dog>();

        //gui notes
     

        
        noteGui1.SetActive(false);
        noteGui1B.SetActive(false);

        noteGui2.SetActive(false);
        noteGui2B.SetActive(false);

        noteGui3.SetActive(false);
        noteGui3B.SetActive(false);

        noteGui4.SetActive(false);
        noteGui4B.SetActive(false);

        noteGui5.SetActive(false);
        noteGui5B.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y> 0.3f)
            transform.position = new Vector3(this.transform.position.x, 0.2f, this.transform.position.z);

        OpenKeyboard();

        pushDog();

        CloseDoors();
        CallDogToSearch();

        PlayAnims();

        PickUp();
        OpenDoors();
        CloseDoors();
        playerDeath();

        if (GameManager.Instance.restartanim)
        {
            restartAnim();
            GameManager.Instance.restartanim = false;
        }

        currentSpeed = GetComponent<Rigidbody>().velocity.magnitude;
        if (currentSpeed < 0.5)
        {
            playerAnims.SetBool("isWalking", false);
        }
        if (gameObject.transform.position.y > 0.1f)
        {

        }
        
       
    }
    private void FixedUpdate()
    {
        MovePlayer();
        speedCheck = (transform.position - lastPosition).magnitude;
        lastPosition = transform.position;
    }


    void PickUp()
    {        



        if (!GameManager.Instance.switchCharacter)
        {
            if (canPickupN1)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    cantmove = true;
                    isPickingN1 = true;
                }
            }
            if (canPickupN2)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    cantmove = true;
                    isPickingN2 = true;
                }
            }
            if (canPickupN3)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    cantmove = true;
                    isPickingN3 = true;
                }
            }
            if (canPickupN4)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    cantmove = true;
                    isPickingN4 = true;
                }
            }
            if (canPickupN5)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    cantmove = true;
                    isPickingN5 = true;
                }
            }
        }

        if (isPickingN1)
        {
            if (nota1 == null)
                nota1 = (GameObject)GameObject.FindGameObjectWithTag("nota1");

          


            
            // pickanim.SetActive(true);

            

           // if (timerPick >= 4.5f)
            //{
              
                //pickanim.SetActive(false);
                //cantmove = false;
                isPickingN1 = false;
                
                Destroy(nota1.gameObject);
                canPickupN1 = false;

                noteGui1.SetActive(true);

                if (noteGui1B != null)
                    noteGui1B.SetActive(true);

                ResetTimers();


           // }

            
            
        }

        if (isPickingN2)
        {
            if (nota2 == null)
                nota2 = (GameObject)GameObject.FindGameObjectWithTag("nota2");

           // timerPick2 += Time.deltaTime;
            

           // if (timerPick2 >= 4.5f)
            //{
               
                
               // cantmove = false;
                isPickingN2 = false;

                Destroy(nota2.gameObject);
                canPickupN2 = false;

                noteGui2.SetActive(true);

                if(noteGui2B != null)
                    noteGui2B.SetActive(true);
                ResetTimers();

            //}



        }

        if (isPickingN3)
        {
            if (nota3 == null)
                nota3 = (GameObject)GameObject.FindGameObjectWithTag("nota3");

            //timerPick3 += Time.deltaTime;
           
          

          //  if (timerPick3 >= 4.5f)
           // {
               
               
               // cantmove = false;
                isPickingN3 = false;

                Destroy(nota3.gameObject);
                canPickupN3 = false;

                noteGui3.SetActive(true);
                if (noteGui3B != null)
                    noteGui3B.SetActive(true);
                ResetTimers();
           // }



        }

        if (isPickingN4)
        {
            if (nota4 == null)
                nota4 = (GameObject)GameObject.FindGameObjectWithTag("nota4");

            timerPick4 += Time.deltaTime;
           
            

           // if (timerPick4 >= 4.5f)
           // {
                
              
                //cantmove = false;
                isPickingN4 = false;

                Destroy(nota4.gameObject);
                canPickupN4 = false;
                noteGui4.SetActive(true);
                if (noteGui4B != null)
                    noteGui4B.SetActive(true);
                ResetTimers();
           // }



        }

        if (isPickingN5)
        {
            if (nota5 == null)
                nota5 = (GameObject)GameObject.FindGameObjectWithTag("nota5");

            timerPick5 += Time.deltaTime;
            

           // if (timerPick5 >= 4.5f)
            //{
                
                
               // cantmove = false;
                isPickingN5 = false;

                Destroy(nota5.gameObject);
                canPickupN5 = false;
                noteGui5.SetActive(true);
                if (noteGui5B != null)
                    noteGui5B.SetActive(true);
                ResetTimers();
           // }



        }

        if (cangrabLabkey)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                //GameManager.Instance.haveKeyFromRoom5 = true;
                GameManager.Instance.haveLabKey = true;
                Destroy(LabKey.gameObject);
            }
        }

        if (cangrabkey41)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
               // GameManager.Instance.haveKeyFromRoom5 = true;
                GameManager.Instance.haveKey41 = true;
                Destroy(key41.gameObject);
            }
        }
        if (cangrabkey42)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                //GameManager.Instance.haveKeyFromRoom5 = true;
                GameManager.Instance.haveKey42 = true;
                Destroy(key42.gameObject);
            }
        }

        if (cangrabCura)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                GameManager.Instance.haveCura = true;
               
                Destroy(cura.gameObject);
            }
        }
    }

    void MovePlayer()
    {
        if (!GameManager.Instance.switchCharacter)
        {
            if (!cantmove)
            {
                CharacterController controller = GetComponent<CharacterController>();

                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= speed;




                controller.Move(moveDirection * Time.deltaTime);
            }
           
        }
    }

    void CallDogToSearch()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (!cantmove)
            {
                if (Dog.isRoom5 == true || Dog.isRoom3 == true)
                {
                    Dog.search = true;

                }
            }
        }
    }

    private void MakeDoorNoise()
    {
        DoorSound.onceKnock = true;
    }

    void OpenDoors()
    {
        if ((Ph1.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {
            canOpen1 = true;

        }
        else
        {
            canOpen1 = false;

        }

        if ((Ph3.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {
            canOpen3 = true;


        }
        else
        {
            canOpen3 = false;

        }

        if ((Ph4.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {
            canOpen4= true;


        }
        else
        {
            canOpen4 = false;

        }

        if ((Ph5.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {
            canOpen5 = true;
            

        }
        else
        {
            canOpen5 = false;

        }

        if ((Ph6.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {
            canOpen6 = true;


        }
        else
        {
            canOpen6 = false;

        }
        if ((Ph7.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {
            canOpen7 = true;


        }
        else
        {
            canOpen7 = false;

        }

        if (canOpen1)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                if (GameManager.Instance.canOpenDoorH1)
                {

                    
                    if(!door1IsOpen)
                        MakeDoorNoise();
                    Ph1.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, 0.0f, transform.position.y));
                    Destroy(th1.gameObject);
                    door1IsOpen = true;
                }
            }
        }
        if (canOpen2)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                if (OpenPh2)
                {
                    DoorSound.onceLocked = true;

                }
            }
        }
        if (canOpen3)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                
                Ph3.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, 0.0f, transform.position.y));
                Destroy(th3.gameObject);
                if(!door3IsOpen)
                    MakeDoorNoise();

                door3IsOpen = true;
            }
        }
       

        if (canOpen4)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                if (GameManager.Instance.canGoGetCode)
                {
                   
                    Ph4.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                    Destroy(th4);
                    if(!door4IsOpen)
                        MakeDoorNoise();
                    door4IsOpen = true;

                }
            }
        }

        if (canOpen5)
        {
           
            if (Input.GetKeyUp(KeyCode.E))
            {
                Ph5.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.rotation.y));
                Destroy(th5.gameObject);

                if(!door5IsOpen)
                    MakeDoorNoise();
                Ph5.GetComponent<Collider>().enabled = false;
                door5IsOpen = true;
            }
        }
        if (canOpen6)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                if (canOpen6)
                {
                    door6IsOpen = true;
                    Ph6.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                    Destroy(th6.gameObject);
                    Ph6.GetComponent<Collider>().enabled = false;
                    MakeDoorNoise();
                }
            }
        }
         if (canOpen7)
            {
                if (Input.GetKeyUp(KeyCode.E))
                {
                    if (GameManager.Instance.haveKeyFromRoom5)
                    {

                       
                       Ph7.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                        GameManager.Instance.canOpenDoorH1 = true;
                        Destroy(th7.gameObject);
                        GameManager.Instance.haveKeyFromRoom5 = false;
                    if(!door7IsOpen)
                        MakeDoorNoise();
                       door7IsOpen = true;
                }
                }
            }
         //level2
        if (canOpen2dr1)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                l2dr1.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                MakeDoorNoise();
            }
        }
        if (canOpen2dr2)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                l2dr2.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                MakeDoorNoise();
            }
        }
        if (canOpen2dr3)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                l2dr3.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, 0.0f, transform.position.y));
                MakeDoorNoise();
            }
        }
        if (canOpen2dr4)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                l2dr4.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, 0.0f, transform.position.y));
                MakeDoorNoise();
            }
        }

        if(canopenCura)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                puertaCura.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                MakeDoorNoise();
            }

        }

        if (canopen42)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                puerta42.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, -90.0f, transform.position.y));
                MakeDoorNoise();
            }

        }
    }
    void CloseDoors()
    {

       /*if(canClose5 && door5IsOpen)
        {
            if (Input.GetKeyUp(KeyCode.E))
            {
                Ph5.gameObject.transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, 0.0f, transform.rotation.y));
               
                MakeDoorNoise();
                door5IsOpen = false;
                canClose5 = false;
            }
        }*/

    }
    private void OnCollisionEnter(Collision collision)
    {

        //OpeningDoors

        if (collision.gameObject.tag == "lockeddoor")
        {
            canOpen2 = true;
        }
            if (collision.gameObject.tag == "Ph7")
        {
            //Ph6.GetComponent<Animation>().Play();
            canOpen7 = true;
        }
        if (collision.gameObject.tag == "Ph6")
        {
            canOpen6 = true;
            
        }

        /*if (collision.gameObject.tag == "Ph5")
        {
            canOpen5 = true;
            Debug.Log("Door");

        }*/

        if (collision.gameObject.tag == "Ph4")
        {
            canOpen4 = true;
        }

        if (collision.gameObject.tag == "Ph3")
        {
            canOpen3 = true;           
            
        }

        if (collision.gameObject.tag == "Ph2")
            canOpen2 = true;

        if (collision.gameObject.tag == "Ph1")
        {
            /* if (GameManager.Instance.canOpenDoorH1)
             { 


             }*/
            canOpen1 = true;
        }

        if (collision.gameObject.tag == "BatteriaH1")
        {
            GameManager.Instance.haveTheBaterieFromRoom1 = true;
            Destroy(collision.gameObject);

        }

        if (collision.gameObject.tag == "putBatteria")
        {
         /*   if (GameManager.Instance.haveTheBaterieFromRoom1)
            { 
                GameManager.Instance.showBatteriaInRoom5 = true;
                
            }*/
            
        }
       

        if (collision.gameObject.tag == "LabKey")
        {
            cangrabLabkey = true;


        }
        if (collision.gameObject.tag == "llave41")
        {
            cangrabkey41 = true;
        }

        if (collision.gameObject.tag == "cura")
        {
            cura = (GameObject)GameObject.FindGameObjectWithTag("cura");
            cangrabCura = true;
        }

        if (collision.gameObject.tag=="exitlevel2")
        {
            GameManager.Instance.isLevel2 = false;
            GameManager.Instance.isLevel3 = true;
        }
       if(collision.gameObject.tag == "exitlevel3")
        {
            if (GameManager.Instance.haveLabKey)
            {
                GameManager.Instance.isLevel3 = false;
                GameManager.Instance.isLevel4 = true;
            }
        }

    }
    private void OnCollisionExit(Collision collision)
    {
      

        if(collision.gameObject.tag == "exitlevel3")
        {
            if (GameManager.Instance.haveLabKey)
            {
                GameManager.Instance.isLevel3 = false;
                GameManager.Instance.isLevel4 = true;
            }
        }
        if (collision.gameObject.tag == "llave41")
        {
            cangrabkey41 = false;
        }

        if (collision.gameObject.tag == "cura")
        {
            cangrabCura = false;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //OpeningDoors

        if (other.gameObject.tag == "ElevatorDoors")
        {
            GameManager.Instance.LoadLevels();
        }


        if (other.gameObject.tag == "lockeddoor")
        {
            canOpen2 = true;
        }
       //level2
        if (other.gameObject.tag == "2dr1")
        {
            l2dr1 = (GameObject)GameObject.FindGameObjectWithTag("2dr1");
            canOpen2dr1 = true;

           

        }
        if (other.gameObject.tag == "2dr2")
        {
            l2dr2 = (GameObject)GameObject.FindGameObjectWithTag("2dr2");

            canOpen2dr2 = true;



        }
        if (other.gameObject.tag == "2dr3" )
        {

            l2dr3 = (GameObject)GameObject.FindGameObjectWithTag("2dr3");
            canOpen2dr3 = true;

        }
        if (other.gameObject.tag == "2dr4")
        {

            l2dr4 = (GameObject)GameObject.FindGameObjectWithTag("2dr4");
            canOpen2dr4 = true;

        }

        if (other.gameObject.tag == "TriggerH4Code")
        {
            GameManager.Instance.havePassword = true;
            
          //  Destroy(textCode.gameObject);
        }

        if(other.gameObject.tag == "IntroducePass")
        {
            GameManager.Instance.showHavePass = true;
        }

        if (other.gameObject.tag == "TriggerKeyboard") 
        {
           /* if(GameManager.Instance.showBatteriaInRoom5)
                GameManager.Instance.showKeyboard = true;*/
        }

        if (other.gameObject.tag == "nota1")
            canPickupN1 = true;
        if (other.gameObject.tag == "nota2")
            canPickupN2 = true;
        if (other.gameObject.tag == "nota3")
            canPickupN3 = true;
        if (other.gameObject.tag == "nota4")
            canPickupN4 = true;
        if (other.gameObject.tag == "nota5")
            canPickupN5 = true;

        if (other.gameObject.tag == "llave42")
        {
            cangrabkey42 = true;
        }
        if (other.gameObject.tag == "puerta42")
        {

            puerta42 = (GameObject)GameObject.FindGameObjectWithTag("puerta42");
            if(GameManager.Instance.haveKey41)
                canopen42 = true;

        }
        if (other.gameObject.tag == "puertaCura")
        {

            puertaCura = (GameObject)GameObject.FindGameObjectWithTag("puertaCura");
            if(GameManager.Instance.haveKey41 && GameManager.Instance.haveKey42)
                canopenCura = true;

        }


    }

    private void OnTriggerExit(Collider exit)
    {
       
        if (exit.gameObject.tag == "llave42")
        {
            cangrabkey42 = false;
        }
        if (exit.gameObject.tag == "puerta42")
        {          
            canopen42 = false;
        }
        if (exit.gameObject.tag == "puertaCura")
        {
            canopenCura = false;
        }
        if (exit.gameObject.tag == "TriggerKeyboard") 
        {
           // GameManager.Instance.showKeyboard = false;
        }

        if (exit.gameObject.tag == "nota1")
            canPickupN1 = false;
        if (exit.gameObject.tag == "nota2")
            canPickupN2 = false;
        if (exit.gameObject.tag == "nota3")
            canPickupN3 = false;
        if (exit.gameObject.tag == "nota4")
            canPickupN4 = false;
        if (exit.gameObject.tag == "nota5")
            canPickupN5 = false;

        if (exit.gameObject.tag == "2dr1")
        {
            canOpen2dr1 = false;



        }
        if (exit.gameObject.tag == "2dr2")
        {
           

            canOpen2dr2 = false;



        }
        if (exit.gameObject.tag == "2dr3")
        {

            
            canOpen2dr3 = false;

        }
        if (exit.gameObject.tag == "2dr4")
        {

            
            canOpen2dr4 = false;

        }
    }

    private void ResetTimers()
    {
        timerPick = 0.0f;
    }

    private void playerDeath()
    {
        
        if (GameManager.Instance.playerplaydead)
        {
           cantmove = true;
           

            /*timeD += Time.deltaTime;

            if (timeD > 2.0f)
            {
                //deathanim.SetActive(false);
                cantmove = false;
            }*/

        }

        
    }
    private void restartAnim()
    {
        
    }

    void PlayAnims()
    {
        if(speedCheck>0.0f)
            playerAnims.SetBool("isWalking", true);
        else
            playerAnims.SetBool("isWalking", false);
       
      }

    void OpenKeyboard()
    {
        if ((TriggerBoard.transform.position - this.transform.position).sqrMagnitude <= minDistance * minDistance)
        {

            if (GameManager.Instance.showBatteriaInRoom5)
                GameManager.Instance.showKeyboard = true;

            if (GameManager.Instance.haveTheBaterieFromRoom1)
            {
                GameManager.Instance.showBatteriaInRoom5 = true;

            }


        }
        else
        {
            if (GameManager.Instance.showBatteriaInRoom5)
                GameManager.Instance.showKeyboard = false;

        }

    }
    void pushDog()
    {
        float ddist = 1.1f;
       if ((rex.transform.position - this.transform.position).sqrMagnitude <= ddist * ddist)
        {

            rex.gameObject.GetComponent<Collider>().enabled = false;
         

        }else
            rex.gameObject.GetComponent<Collider>().enabled = true;
    }
}