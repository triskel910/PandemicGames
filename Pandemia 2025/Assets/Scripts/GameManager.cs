﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public bool switchCharacter = false;

    private GameObject[] LightTextures;

    public bool haveKeyFromRoom5 = false;
    public bool haveTheBaterieFromRoom1 = false;
    public bool havePassword = false;
    public bool showHavePass = false;

    public bool canOpenDoorH1 = false;

    private GameObject BatteriaInRoom5;
    public bool showBatteriaInRoom5 =false;
    public GameObject ElektrikFX;
    private GameObject EFX;
    private bool showfxE = false;

    public GameObject askPass;
    public GameObject showPassF;
    public bool canGoGetCode = false;

    public GameObject IntroducePassT;

    public GameObject light;

    public bool respawnKey = true;
    

    private bool canGoElevator = false;

    private GameObject Lights;


    private Dog DogFunctions;

    public GameObject generadorSound;


    private GameObject Keyboard;
    public bool showKeyboard = false;
    public string password = "";
   
    public float randomPassword;

    private GameObject player;
    private GameObject Dogs;

   

    //GUI
    private GameObject showGuiBateria;
    private GameObject showGuiKey;
    public GameObject showGuiLabKey;
    public GameObject showGui41;
    public GameObject showGui42;



    //Level2
    public GameObject spawnpoint2;
    

    //Level3
    public GameObject spawnpoint3;
    public bool haveLabKey = false;

    //level4
    public GameObject spawnpoint4;


    //load levels

    private int ChangeLevelsL = 0;

    public bool isLevel1 = true;
    public bool isLevel2 = false;
    public bool isLevel3 = false;
    public bool isLevel4 = false;
    // public GameObject Level2;
    private GameObject genLevel2;
    private GameObject genLevel3;
    private GameObject genLevel4;
    private bool loadLevel2 = false;
    private bool loadLevel3 = false;
    private bool loadLevel4 = false;

    private float timerspawn = 0.0f;
    private float timerspawn2 = 0.0f;
    private float timerspawn3 = 0.0f;
    private bool maketeleport2 = false;

   
    
    public bool playerplaydead = false;

    private float backtomenu = 0.0f;

    public bool haveKey41 = false;
    public bool haveKey42 = false;

    public bool haveCura = false;

    public GameObject DeathMenu;
    public bool showDeathmenu;

    public GameObject PauseMenu;
    public bool isPaused  = false;
    
    public bool restartZombiesLevel2;
    public bool restartZombiesLevel3;
    public bool restartZombiesLevel4;

    public bool restartanim;

    public GameObject SmokeFx;

    public bool freezeTime = false;
    public GameObject showObjectives;
    private int sobjectives;

    private int objectiveNumb = 0;
    public GameObject fo1;
    public GameObject fo2;
    public GameObject fo3;
    public GameObject fo4;

    public GameObject ObjectiveList1;
    public GameObject ObjectiveList2;
    public GameObject ObjectiveList3;
    public GameObject ObjectiveList4;

    public GameObject clickContinue2;
    public GameObject clickContinue3;
    public GameObject clickContinue4;

    public GameObject LoadScreen2;
    public GameObject LoadScreen3;
    public GameObject LoadScreen4;
    
    public bool showLoadScreen2 = false;
    public bool showLoadScreen3 = false;
    public bool showLoadScreen4 = false;
    public GameObject audioLevel2;
    public GameObject audioLevel3;
    public GameObject audioLevel4;

    public bool playLoad2;
    public bool playLoad3;
    public bool playLoad4;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }

    }

    // Use this for initialization
    void Start () {
    
        PauseMenu.SetActive(false);
        showObjectives.SetActive(false);
        ObjectiveList2.SetActive(false);
        ObjectiveList3.SetActive(false);
        ObjectiveList4.SetActive(false);

        clickContinue2.SetActive(false);
        clickContinue3.SetActive(false);
        clickContinue4.SetActive(false);


        audioLevel2.SetActive(false);
        audioLevel3.SetActive(false);
        audioLevel4.SetActive(false);

        LoadScreen2.SetActive(false);
        LoadScreen3.SetActive(false);
        LoadScreen4.SetActive(false);


        generadorSound.SetActive(false);

        DeathMenu.SetActive(false);

        player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        Dogs = (GameObject)GameObject.FindGameObjectWithTag("Dog");

        Keyboard = (GameObject)GameObject.FindGameObjectWithTag("showKeyboard");

        showGuiBateria = (GameObject)GameObject.FindGameObjectWithTag("ShowGuiBateria");
        showGuiKey = (GameObject)GameObject.FindGameObjectWithTag("ShowGuiKey");


        LightTextures = GameObject.FindGameObjectsWithTag("lightsOn");

        foreach (GameObject LightTexture in LightTextures)
        {
            LightTexture.SetActive(false);
        }

            showGuiBateria.SetActive(false);
        showGuiKey.SetActive(false);

        makePassword();

        Keyboard.SetActive(false);

      

        BatteriaInRoom5 = (GameObject)GameObject.FindGameObjectWithTag("BatteriaH5");

        Lights = (GameObject)GameObject.FindGameObjectWithTag("Lights");

        BatteriaInRoom5.SetActive(false);

        askPass.SetActive(false);
       

        light.GetComponent<Light>().intensity = 0;
        Lights.gameObject.SetActive(false);

        DogFunctions = FindObjectOfType<Dog>();

        


        playerplaydead = false;
        //level2

        fo2.SetActive(false);
        fo3.SetActive(false);
        fo4.SetActive(false);


    }
	
	// Update is called once per frame
	void Update () {

        if (showDeathmenu)
            DeathMenu.SetActive(true);
        if (!showDeathmenu)
            DeathMenu.SetActive(false);

        if(showLoadScreen2)
            LoadScreen2.SetActive(true);
        else
        {
            LoadScreen2.SetActive(false);
            clickContinue2.SetActive(false);
        }
        if (showLoadScreen3)
            LoadScreen3.SetActive(true);
        else
        {
            LoadScreen3.SetActive(false);
            clickContinue3.SetActive(false);
        }
        if (showLoadScreen4)
            LoadScreen4.SetActive(true);
        else
        {
            LoadScreen4.SetActive(false);
            clickContinue4.SetActive(false);
        }
        WhenPlayerDead();


        //GUI bool

        if (haveTheBaterieFromRoom1)
            showGuiBateria.SetActive(true);

        if (showBatteriaInRoom5)
            BatteriaInRoom5.SetActive(true);

        if (haveKeyFromRoom5)
        {
            showGuiKey.SetActive(true);
        }
        else
        {
            showGuiKey.SetActive(false);
        }

        if(haveLabKey)
            showGuiLabKey.SetActive(true);
        else
            showGuiLabKey.SetActive(false);

        if (haveKey41)
            showGui41.SetActive(true);
        else
            showGui41.SetActive(false);

        if (haveKey42)
            showGui42.SetActive(true);
        else
            showGui42.SetActive(false);

        IntroducePassword();

      

       if(showKeyboard)
            Keyboard.SetActive(true);

        if (!showKeyboard)
            Keyboard.SetActive(false);      
        

        if(Input.GetKeyDown(KeyCode.L))
        {
           
            ChangeLevelsL++;

            if (ChangeLevelsL == 1)
            {
                isLevel2 = true;
                isLevel1 = false;
            }

            if (ChangeLevelsL == 2)
            {
                isLevel3 = true;
                isLevel2 = false;
            }

            if (ChangeLevelsL == 3)
            {
                isLevel4 = true;
                isLevel3 = false;
            }

          


        }
        

        if (isLevel2)
        {
            loadLevel2 = true;
           
        }
       

        if (loadLevel2)
          {
            gLevel2();
           
          }

        if (isLevel3)
        {
            loadLevel3 = true;
            audioLevel2.SetActive(false);
            clickContinue2.SetActive(false);

        }
        if (loadLevel3)
        {
           
           // gLevel3();
        }


        if (isLevel4)
        {
            haveLabKey = false;
            loadLevel4 = true;
            audioLevel3.SetActive(false);
            clickContinue3.SetActive(false);
        }

        if (loadLevel4)
           // gLevel4();


        if(showfxE)
        {
            if (EFX == null)
            {
                EFX = Instantiate(Resources.Load("EFX")) as GameObject;
            }
            
            showfxE = false;
        }

        ShowObjectives();
        stopTime();


        Pause();


        //LoadEnd Cinamitic
        if (haveCura)
            Application.LoadLevel(2);


        if(playLoad2)
        {
            showLoadScreen2 = true;
            audioLevel2.SetActive(true);
            StartCoroutine(ShowClickContinue2(6f));
        }
        if(playLoad3)
        {
            showLoadScreen2 = false;
            audioLevel2.SetActive(false);
            showLoadScreen3 = true;
            audioLevel3.SetActive(true);
            StartCoroutine(ShowClickContinue3(6f));
        }
        if(playLoad4)
        {
            showLoadScreen4 = true;
            audioLevel4.SetActive(true);
            StartCoroutine(ShowClickContinue4(6f));
        }
    }



    void IntroducePassword()
    {
        if(showBatteriaInRoom5 && haveTheBaterieFromRoom1)
        {
            showGuiBateria.SetActive(false);
            showfxE = true;
            Destroy(EFX, 3.0f);
            askPass.SetActive(true);
            canGoGetCode = true;
           
        }
        if (askPass)
        {                      
            if (string.Equals(InputText.currentPassword, randomPassword.ToString()))
            {
                SmokeFx.SetActive(true);
                generadorSound.SetActive(true);
                canGoElevator = true;
                showKeyboard = false;
                Lights.gameObject.SetActive(true);
                foreach (GameObject LightTexture in LightTextures)
                {
                    LightTexture.SetActive(true);
                }
            }

            }

        }

   public void LoadLevels()
    {

        if (canGoElevator)
        {            
            isLevel2 = true;
            isLevel1 = false;
        }

    }

    private void makePassword()
    {     
        randomPassword = Random.Range(1000, 9999);           
        Mathf.RoundToInt(randomPassword);
        Debug.Log("rpass= " + randomPassword);
    }

    public void gLevel2()
    {
        Application.LoadLevel(3);
    }

    public void WhenPlayerDead()
    {
        
        if (playerplaydead)
        {
            StartCoroutine(StartLoading(2f));
            
        }
    }
    IEnumerator StartLoading(float value)
    {
        yield return new WaitForSeconds(value);
        playerplaydead = false;
        showDeathmenu = true;
        GameManager.Instance.restartanim = true;
        Player.cantmove = false;
        if (isLevel2)
        {
            
         
        }

        if (isLevel3)
        {

           
        }

        if (isLevel4)
        {

           
        }
    }
    IEnumerator ShowClickContinue2(float v)
    {
        yield return new WaitForSeconds(v);
        
            clickContinue2.SetActive(true);       

    }
    IEnumerator ShowClickContinue3(float v)
    {
        yield return new WaitForSeconds(v);

        clickContinue3.SetActive(true);


    }
    IEnumerator ShowClickContinue4(float v)
    {
        yield return new WaitForSeconds(v);

        clickContinue4.SetActive(true);


    }
    public void ShowObjectives()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
           
          if(!showObjectives.gameObject.activeInHierarchy)
            {
                showObjectives.SetActive(true);
                freezeTime = true;
            }
           else
            {
                showObjectives.SetActive(false);
                freezeTime = false;
            }
          

        }

        if(isLevel2)
        {
            ObjectiveList1.SetActive(false);
            ObjectiveList2.SetActive(true);
            fo1.SetActive(true);
            fo2.SetActive(false);
            fo3.SetActive(false);
            fo4.SetActive(false);
        }
        if(isLevel3)
        {
            fo1.SetActive(false);
            ObjectiveList2.SetActive(false);
            ObjectiveList3.SetActive(true);
        }
        if(isLevel4)
        {
            ObjectiveList3.SetActive(false);
            ObjectiveList4.SetActive(true);
        }

         
        if (haveKeyFromRoom5)
        {
            fo1.SetActive(false);
            fo2.SetActive(true);

        }
        

        if(canGoGetCode)
        {
            fo2.SetActive(false);
            fo3.SetActive(true);
        }


        if(canGoElevator)
        {
            fo3.SetActive(false);
            fo4.SetActive(true);
        }

    }
    public void stopTime()
    {
        if (freezeTime)
            Time.timeScale = 0.0f;

        if (!freezeTime)
            Time.timeScale = 1.0f;
    }
    public void Pause()
    {
        if (!isPaused)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                freezeTime = true;                
                PauseMenu.SetActive(true);
                isPaused = true;
            }
        }
    }
    public void Resume()
    {
        freezeTime = false;
        isPaused = false;
        PauseMenu.SetActive(false);
    }
}
